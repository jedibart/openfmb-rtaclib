# RTAC OpenFMB

## Prerequisites for working with this library

*NOTE: replace instances of OpenFMBtransport library __version__ in these instructions
with the version of the library found in the `./OpenFMBtransportBinary` folder.
(The version will always be a 4 digit version of the form, for example: 0.0.0.0)

1. Install a version of the [SEL AcSELerator RTAC software](https://selinc.com/products/5033/)
    that supports R148 firmware or newer.
    - Check to ensure that the `device.xml` file in both paths of the AcSELerator RTAC install:
      `C:\ProgramData\SEL\AcSELerator\RTAC\Logic Engine\Devices\4096\1031 3350\1.4.8.0`
      `C:\ProgramData\SEL\AcSELerator\RTAC\Logic Engine\Devices\4096\1031 3350\1.4.8.0`
      contains the XML fragment:

      ```xml
      <ts:setting name="OpenFMBtransport" type="string" access="visible">
        <ts:value>OpenFMBtransport, __version__ (SEL)</ts:value>
      </ts:setting>
      ```

      Inside the XML tree location:

      ```xml
      <DeviceDescription>
        <Device>
          <ExtendedSettings>
            <ts:TargetSettings>
              <ts:section name="library-management">
                <ts:section name="placeholder-libraries">
      ```

      If not, add the fragment to both files.

2. Open up AcSELerator RTAC and Install the OpenFMBtransport library (if not already done)
    `<SEL> <ManageLibraries> <Install New ...>`  Select the `./OpenFMBtransportBinary/OpenFMBtransport-__version__.compiled-library`
3. Ensure that AcSELerator RTAC has "Enable Developer Mode" enabled in the options
   `<SEL> <Options> <Preferences> --> Enable Developer Mode (requires restart) is checked`
4. Have an RTAC device that has the R148+OpenFMB firmware loaded on it to support the
   runtime use of the OpenFMBtransport library (only required for actually running and
   testing the library.)

## How to import Library for Development in AcSELerator RTAC

The `CoDeSysLibs` folder contains the code and meta data for a library. The code
is saved as AcSELerator RTAC xml export.

### Importing xml to AcSELerator RTAC Project

1. Open AcSELerator RTAC
2. Create New SEL RTAC Project
3. Select the RTAC type and RTAC Firmware Version you will be running tests
    during development - This selection is only used during development.
    A project for each RTAC type and RTAC Firmware Version must be created in order
    to run tests on different hardware platforms.
4. Select `Create As Library` option which will open the editor in a mode which supports
    some subtle abilities available in libraries that are meant to be used in other "final"
    projects. *(Notably, enables the creation of global parameter lists, not commonly used)*
5. Select XML under Project type
6. Change the XML Folder source to the `CoDeSysLibs` folder of
    this library
7. Enter an appropriate Project Name
8. Click Create

### Exporting AcSELerator RRTAC Project to xml

These steps assume the project to export is open in AcSELerator RTAC

1. Click the SEL menu button
2. Export Items
3. Select the parts of the project to export.
    - Note: AcSELerator RTAC will prompt to overwrite any files that already exist
4. Change the Export Location to the `CoDeSysLibs` folder of this
    library.
5. Click Export
6. Verify Export contents

## Using the OpenFMB library in another project for testing

In order to validate or test the functionality of the OpenFMB library under development,
install the working library into the managed library repository. This can be either by:

### Option 1. Saving and Installing the library (used primarily for sharing)

This workflow involves actually exporting the library as though it is going to be shared,
and then installing it back into the library repository managed by AcSELerator RTAC.

From within the OpenFMB project that is being worked on:

1. Select `<Advanced> <POU> <Save POUs as .compiled library>` - pick a filename
2. In the instance of the AcSELerator RTAC where you wish to create a project using this library,
    install the library:
     `<SEL> <ManageLibraries> <Install New ...>` -- Select the saved file from step 1
3. Include the library in the project:
     `<Insert> <IEC61131-3> <Library>` -- Select the installed library

To update the library, repeat the steps. The same library version can be installed overtop of the older one,
but consider bumping the version number in the project and filenames to keep track if you're moving the library
around at all.

### Option 2. (Preferred) Installing from the editor

This workflow is much more convenient when working on the library iteratively.
It works best if there are two instances of AcSELerator RTAC open, one with the library,
and one with the project file that is using the library as an example (on the same development machine).
This example assumes that is the case.

1. In the first AcSELerator RTAC instance with the library imported:
    Install the working copy of the library directly into the library repository:
    `<Advanced> <POU> <Install POUs as library>`
2. In the other instance of the AcSELerator RTAC project with the example project to use the library,
    include the library in the project:
    `<Insert> <IEC61131-3> <Library>` -- Select the installed library

To update the library now, simply repeat step 1 every time a change is made to the library.
The other AcSELerator RTAC project will recognize the change, and will use the new library version
the next time it is compiled (with `<ctrl> + <s>` or `<f6>`).
